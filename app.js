const express = require('express');
const path = require('path');
const compression = require('compression');
const bodyParser = require('body-parser');

const sqlRead = require('./sql-read');

const app = express();
// Can be omitted, if Module ngx_http_gzip_module enabled in nginx
app.use(compression());

//'Access-Control-Allow-Origin' header
const cors = require('cors');
app.use(cors());

// Switch off the default 'X-Powered-By: Express' header
app.disable('x-powered-by');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.static(path.join(__dirname, 'src')));

app.get('/users', function(req, res) {
    //console.log(req.body);
    const users = sqlRead.getUsers();
    res.send({
        users
    })
});

app.post('/getuserstate', function(req, res) {
    console.log(req.body);
    let state = sqlRead.getUserState(req.body.id);
    res.send({
        state
    });

});

app.post('/getchefstate', function(req, res) {
    console.log(req.body);
    let state = sqlRead.getChefState(req.body.id);
    console.log(state);
    res.send({
        state
    });

});
app.post('/setfoodprefs', function(req, res) {
    console.log('setfoodprefs');
    console.log(req.body);
    let ret = sqlRead.setUserFoodPreference(req.body.id, req.body.option);
    res.send({
        success: ret
    });

});
app.post('/getfoodpref', function(req, res) {
    console.log('getfoodpref');
    console.log(req.body);
    let data = sqlRead.getUserFoodPreference(req.body.id);
    res.send({
        data
    });

});

app.post('/getusermeal', function(req, res) {
    console.log('getusermeal');
    console.log(req.body);
    let data = sqlRead.getUserMeal(req.body.id);
    res.send({
        data
    });

});

app.post('/freezemeals', function(req, res) {
    console.log('freezemeals');
    console.log(req.body);
    let data = sqlRead.setChefMeals(req.body.data);
    res.send({
        data
    });

});

app.post('/setusermeal', function(req, res) {
    console.log('setusermeal');
    console.log(req.body);
    let data = sqlRead.setUserMeal(req.body.id, req.body.option);
    res.send({
        data
    });

});


app.get('/getfoodprefs', function(req, res) {
    let data = sqlRead.getFoodPreferences();
    res.send({
        data
    });

});

app.get('/getsuggestedmeals', function(req, res) {
    let data = sqlRead.getSuggestedMeals();
    res.send({
        data
    });

});

app.get('/getchefmeals', function(req, res) {
    let data = sqlRead.getChefMeals();
    res.send({
        data
    });

});
app.get('/getorders', function(req, res) {
    let data = sqlRead.getOrders();
    res.send({
        data
    });

});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.redirect('/');
//let err = new Error('Not Found');
//err.status = 404;
//next(err);
});
if (app.get('env') === 'development') {
    console.log("App is Running in development mode");
}

// production error handler
// no stack-traces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: {}
    });
});


module.exports = app;

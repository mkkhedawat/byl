const sql = require('./sql-simulation');

function getUsers() {
    let users = JSON.parse(JSON.stringify(sql.getUsers()));
    const cState = getChefState(1);
    if (cState == 1 || cState == 3 ) {
        users = {
            1: users[1]
        };
    } else if (cState == 0) {
        delete users[1];
        let foodPrefs = sql.getUserFoodPreferences();
        for (let key in foodPrefs) {
            if (foodPrefs[key] !== -1) {
                delete users[key];
            }
        }
    } else if (cState === 2) {
        delete users[1];
        let meals = sql.getUserMealSelections();
        for (let key in meals) {
            if (meals[key] !== -1 ) {
                delete users[key];
            }
        }
    } else {
        console.log('Error : getUsers()');
        users = {
        };
    }

    //console.log(users);
    return users;
}

function getUser(id) {
    const user = sql.getUser(id);
    return user;
}

function getUserState(id) {
    let state = 0;
    let user = getUser(id);
    //console.log(`check states for ${user[1]}`);
    /*
        0 : You have not selected food preference
        1 - Not all has selected food preference but you
        2 - Chef has not decided the meal yet
        3 - Select meal
        4 - You have selected meal
        */


    // Check for state 1

    let foodPref = sql.getUserFoodPreference(id);
    if (foodPref !== -1) {
        state++;
    } else {
        return state;
    }

    // state = 1
    // Check for state 2;

    let foodPrefs = sql.getUserFoodPreferences();
    for (let key in foodPrefs) {
        if (foodPrefs[key] === -1 && key != 1) {
            return state;
        }
    }
    state++;

    //state = 2
    // Check for state 3
    let chefMeals = sql.getChefMeals();
    //console.log(chefMeals);
    for (let i = 0; i < chefMeals.length; i++) {
        if (chefMeals[i] == -1) {
            return state;
        }
    }
    state++;

    // state = 3
    // Check for state 4

    let meal = sql.getUserMealSelection(id);
    if (meal == -1) {
        return state
    }
    state++;

    // state = 4
    return state;
}

function getChefState(id) {
    // id = 1
    let state = 0;
    let user = getUser(id);
    //console.log(`check states for ${user[1]}`);
    /*
        0 : Not all has selected food preferences
        1 - Decide Meals
        2 - Not all has selected meals
        3 - order lists
        */

    // state = 0
    // Check for state 1;

    let foodPrefs = sql.getUserFoodPreferences();
    for (let key in foodPrefs) {
        if (foodPrefs[key] === -1 && key != 1) {
            return state;
        }
    }
    state++;

    //state = 1
    // Check for state 2
    let chefMeals = sql.getChefMeals();
    for (let i = 0; i < chefMeals.length; i++) {
        if (chefMeals[i] === -1) {
            return state;
        }
    }
    state++;

    // state = 2
    // Check for state 3

    let meals = sql.getUserMealSelections();
    for (let key in meals) {
        if (meals[key] === -1 && key != 1) {
            return state;
        }
    }

    state++;

    // state = 3
    return state;
}

function getFoodPreferences() {
    let data = sql.getFoodPreferences();
    return data;
}

function setUserFoodPreference(id, option) {
    console.log("setUserFoodPreference " + id + ' ' + option);
    let ret = sql.setUserFoodPreference(id, option);
    return ret;
}

function getUserFoodPreference(id) {
    let foodId = sql.getUserFoodPreference(id);
    let food = sql.getFoodPreference(foodId);
    return food;
}

function getSuggestedMeals() {
    const meals = sql.getMeals();
    const mealCat = sql.getMealCategories();
    const pref = sql.getUserFoodPreferences();
    let rMeals = [];

    for (let key in meals) {
        rMeals.push({
            id: key,
            meal: meals[key],
            points: 0
        });
    }
    for (let key in pref) {
        if (key > 1) {
            let p = pref[key];
            let mArr = mealCat[p];
            mArr.forEach((m) => {
                // Id starts from 1
                rMeals[m - 1].points++;
            });
        }
    }

    rMeals = rMeals.sort(function(a, b) {
        return b.points - a.points;
    });

    const data = [];
    for (let i = 0; i < rMeals.length; i++) {
        data.push(rMeals[i].meal);
    }
    sql.setRecommendedMeals(rMeals);
    return data;
}

function getChefMeals() {
    //sql.setChefMeals([1, 2, 3]);
    const meals = sql.getChefMeals();
    const data = [];
    for (let i = 0; i < meals.length; i++) {
        data.push(sql.getMeal(meals[i]));
    }
    return data;
}

function getUserMeal(id) {
    const mealId = sql.getUserMealSelection(id);
    const meal = sql.getMeal(mealId);
    console.log(mealId + '  ' + meal);
    return meal;
}

function setChefMeals(data) {
    const rMeals = sql.getRecommendedMeals();
    const final = [];
    data.forEach((d) => {
        final.push(rMeals[d - 1].id);
    })
    const ret = sql.setChefMeals(final);
    //console.log(rMeals);
    //console.log(ret);
    return ret;
}

function getOrders() {
    const orders = sql.getUserMealSelections();
    console.log(orders);
    const meals = sql.getMeals();

    let oMeals = [];

    for (let key in meals) {
        oMeals.push({
            id: key,
            meal: meals[key],
            count: 0
        });
    }

    for (let key in orders) {
        // index start from 0
        if (orders[key] > -1) {
            oMeals[orders[key] - 1].count++;
        }
    }
    //console.log(oMeals);
    oMeals = oMeals.filter((f) => {
        return f.count > 0;
    });
    //console.log(oMeals);
    oMeals = oMeals.sort((a, b) => {
        return b.count - a.count;
    });

    return oMeals;
}

function setUserMeal(id, mealId) {
    const meals = sql.getChefMeals();
    const ret = sql.setUserMealSelection(id, meals[mealId - 1]);
    return ret;
}

module.exports = {
    getUsers,
    getUser,
    getUserState,
    getChefState,
    getFoodPreferences,
    setUserFoodPreference,
    getUserFoodPreference,
    getSuggestedMeals,
    getChefMeals,
    getUserMeal,
    setChefMeals,
    getOrders,
    setUserMeal
}
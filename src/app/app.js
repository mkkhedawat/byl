import React from 'react'
import { BrowserRouter as Router, Route, Link, browserHistory } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';


/*
  Route Container for Application
*/
import LandingPage from './containers/LandingPage'
import User from './containers/User'
import Chef from './containers/Chef'


export default class App extends React.Component {
  render() {
    return (
      <Router history={ browserHistory }>
        <div>
          <Route exact path='/' component={ LandingPage } />
          <Route path='/user' component={ User } />
          <Route path='/chef' component={ Chef } />
        </div>
      </Router>
    )
  }
}

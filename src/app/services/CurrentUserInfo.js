let currentUser = {
    id: -1,
    name: 'Dummy User'
}

function setCurrentUser(id, name)
{
    console.log(id + ' ' + name);
    currentUser = {
        id,
        name
    }
}
function getCurrentUser()
{
    return currentUser;
}

export default {
    setCurrentUser,
    getCurrentUser
}
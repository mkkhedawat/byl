import React from 'react';
import axios from 'axios';
import CurrentUserInfo from '../services/CurrentUserInfo';
import SelectOptions from '../components/SelectOptions';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
    label: {
        textAlign: 'left',
        color: 'rgb(33, 150, 243)',
        fontSize: '1em',
        fontWeight: 300,
    },
    menu: {
        color: 'rgb(33, 150, 243)',
        fontWeight: 300
    }
};

function getUserStatesHTML(i, cxt) {
    let user = CurrentUserInfo.getCurrentUser();
    /*
        0 : You have not selected food preference
        1 - Not all has selected food preference but you
        2 - Chef has not decided the meal yet
        3 - Select meal
        4 - You have selected meal
        */

    let text = '';
    let html = '';
    switch (i) {
        case 0: {
            axios.get('/getfoodprefs')
                .then(function(response) {
                    console.dir(response.data);
                    const data = response.data.data;
                    const menuItems = [];
                    for (let d in data) {
                        menuItems.push(data[d]);
                    }
                    let html = <div>
                                 <SelectOptions onChange={ cxt.handleChange } menuItems={ menuItems } label="" />
                                 <div className='vspace'></div>
                                 <RaisedButton label="Freeze" primary={ true } onClick={ cxt.handleNext } />
                               </div>;

                    cxt.setState({
                        text: `Hello ${user.name}, Select you food preference`,
                        html
                    });

                })
                .catch(function(error) {
                    console.log(error);
                });


            break;
        }
        case 1: {
            axios.post('/getfoodpref', {
                id: user.id,
            }).then(function(response) {
                console.dir(response.data);
                const data = response.data.data;
                let html = <div className='blue-font'>
                             <div className='vspace'></div>
                             <div>You have selected
                               { ' ' }
                               { data } as your food preference</div>
                             <div>But not all have not entered food preferences yet.</div>
                             <div>Please come back later to place order for meal.</div>
                           </div>;
                cxt.setState({
                    text: '',
                    html
                });

            }).catch(function(error) {
                console.log(error);
            });
            break;
        }
        case 2: {
            let html = <div className='blue-font'>
                         <div className='vspace'></div>
                         <div>Hello
                           { ' ' }
                           { user.name }
                         </div>
                         <div>Chef has not yet decided meals for the day</div>
                         <div>Please come back later to place order for meal.</div>
                       </div>;
            cxt.setState({
                text: '',
                html
            });
            break;
        }
        case 3: {
            axios.get('/getchefmeals')
                .then(function(response) {
                    console.dir(response.data);
                    const data = response.data.data;
                    let html = <div>
                                 <SelectOptions onChange={ cxt.handleChange } menuItems={ data } label="" />
                                 <div className='vspace'></div>
                                 <RaisedButton label="Select" primary={ true } onClick={ cxt.handleNext } />
                               </div>;

                    cxt.setState({
                        text: `Hello ${user.name}, Order meal for the day`,
                        html
                    });

                }).catch(function(error) {
                console.log(error);
            });
            break;
        }
        case 4: {
            axios.post('/getusermeal', {
                id: user.id,
            }).then(function(response) {
                console.dir(response.data);
                const data = response.data.data;
                let html = <div className='blue-font'>
                             <div className='vspace'></div>
                             <div>You have selected
                               { ' ' }
                               { data } as meal</div>
                             <div>looking forward to see you at lunch</div>
                           </div>;
                cxt.setState({
                    text: '',
                    html
                });

            }).catch(function(error) {
                console.log(error);
            });
            break;
        }
        default:
    }

    return '';
}

export default {
    getUserStatesHTML
}

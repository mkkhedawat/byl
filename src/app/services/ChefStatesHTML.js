import React from 'react';
import axios from 'axios';
import CurrentUserInfo from '../services/CurrentUserInfo';
import SelectOptions from '../components/SelectOptions';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
    label: {
        textAlign: 'left',
        color: 'rgb(33, 150, 243)',
        fontSize: '1em',
        fontWeight: 300,
    },
    menu: {
        color: 'rgb(33, 150, 243)',
        fontWeight: 300
    }
};

function getChefStatesHTML(i, cxt) {
    let user = CurrentUserInfo.getCurrentUser();
    /*
        0 : Not all has selected food preferences
        1 - Decide Meals
        2 - Not all has selected meals
        3 - order lists
        */

    let text = '';
    let html = '';
    switch (i) {
        case 0: {
            let html = <div className='blue-font'>
                         <div className='vspace'></div>
                         <div>Not all have not entered their food preferences yet.</div>
                         <div>Please come back later to select meals for the day</div>
                       </div>;
            cxt.setState({
                text: `Hello ${user.name}`,
                html
            });
            break;
        }
        case 1: {
            axios.get('/getsuggestedmeals')
                .then(function(response) {
                    console.dir(response.data);
                    const data = response.data.data;
                    cxt.setState({
                        choice: [data[0],
                            data[1],
                            data[2]],
                        choiceId: [1, 2, 3],
                        menuItems: data
                    });
                    let html = <div>
                                 <SelectOptions onChange={ cxt.handleChange } menuItems={ data } label="" />
                                 <div className='vspace'></div>
                                 <RaisedButton label="Select" primary={ true } onClick={ cxt.handleSelect } />
                                 <div className='vspace'></div>
                               </div>;

                    cxt.setState({
                        text: `Hello ${user.name}, Select meal for the day`,
                        html
                    });

                }).catch(function(error) {
                console.log(error);
            });
            break;
        }
        case 2: {
            cxt.setState({
                text: 'Its time to wait until everybody orders food :)',
                html
            });
            break;
        }
        case 3: {
            axios.get('/getorders')
                .then(function(response) {
                    console.dir(response.data);
                    let data = response.data.data;
                    let html = <div className='blue-font'>
                        <div> Hello {user.name} {', '}Orders are listed Below : </div>
                        {
                            data.map((d) => {
                                return <div>{d.count} -  {d.meal}</div>
                            })
                        }
                    </div>
                    cxt.setState({
                        text: '',
                        html
                    });

                }).catch(function(error) {
                console.log(error);
            });
            break;
        }
        default:
    }

    return '';
}

export default {
    getChefStatesHTML
}

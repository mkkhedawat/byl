import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CurrentUserInfo from '../services/CurrentUserInfo';
import ChefStatesHTML from '../services/ChefStatesHTML';
import RaisedButton from 'material-ui/RaisedButton';
import axios from 'axios';

/*
    Page Container for User
*/

let user;
let optionId;

export default class Chef extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleFreeze = this.handleFreeze.bind(this);
        this.goHome = this.goHome.bind(this);
        user = CurrentUserInfo.getCurrentUser();
        this.state = {
            value: 1,
            text: `Hello ${user.name},
                wait while we fetch info from server`,
            html: '',
            userState: 0,
            choice: ['-No Meal-', ' ', ' '],
            choiceId: [-1, -1, -1],
            html2: ''
        };
    }
    handleChange(value) {
        if (value == optionId) return;
        optionId = value;
    }

    goHome() {
        this.props.history.push('/');
    }

    handleFreeze() {
        let pass = true;
        const cxt = this;
        this.state.choiceId.forEach((c) => {
            if (c == -1) {
                pass = false;
            }
        });
        if (pass) {
            axios.post('/freezemeals', {
                data: cxt.state.choiceId
            }).then(function(response) {
                console.dir(response.data);
                cxt.setState({
                    text: `Hello ${user.name}, wait while we fetch info from server`,
                    html: '',
                })
                cxt.processStates();
            }).catch(function(error) {
                console.log(error);
            });
        }
    }

    handleSelect() {
        //console.log('handleSelect()');
        let pass = true;
        this.state.choiceId.forEach((c) => {
            if (c == optionId)
                pass = false;
        })
        if (!pass || optionId === undefined) return;
        if (this.state.userState == 1) {
            this.setState({
                choice: [
                    this.state.menuItems[this.state.choiceId[1] - 1],
                    this.state.menuItems[this.state.choiceId[2] - 1],
                    this.state.menuItems[optionId - 1]],
                choiceId: [this.state.choiceId[1], this.state.choiceId[2], optionId]
            });
        } else {
            console.log('Unexpected handleSelect()');
        }
    }

    render() {
        return (
            <MuiThemeProvider>
              <div className='row'>
                <h2 className='header-text'> <a href='/'> BYL : Book Your Lunch </a></h2>
                <div className='vspace'></div>
                <h5>{ this.state.text }</h5>
                { this.state.html }
                <div>
                  { this.state.choice.map((c, index) => {
                        if (this.state.userState != 1) {
                            return '';
                        }
                        if (index == 2) {
                            return (
                                <div>
                                  <div key={ index }>
                                    { c } </div>
                                  <div className='vspace'></div>
                                  <RaisedButton label="Freeze" primary={ true } onClick={ this.handleFreeze } />
                                  <div className='vspace'></div>
                                </div>
                            )
                        }
                        return (<div key={ index }>
                                  { c } </div>)
                    }) }
                </div>
                <div className='vspace'></div>
                <RaisedButton label="Go Home" primary={ true } onClick={ this.goHome } />
                <div className='vspace'></div>
              </div>
            </MuiThemeProvider>
            );
    }

    componentDidUpdate() {}
    componentDidMount() {
        this.processStates();
    }

    processStates() {
        const cxt = this;
        axios.post('/getchefstate', {
            id: user.id
        }).then(function(response) {
            console.dir(response.data);
            cxt.setState({
                userState: response.data.state
            })
            ChefStatesHTML.getChefStatesHTML(response.data.state, cxt);
        }).catch(function(error) {
            console.log(error);
        });
    }
}

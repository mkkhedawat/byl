import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CurrentUserInfo from '../services/CurrentUserInfo';
import UserStatesHTML from '../services/UserStatesHTML';
import axios from 'axios';
import RaisedButton from 'material-ui/RaisedButton';

/*
    Page Container for Chef
*/

let user;
let optionId = 1;
export default class User extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.goHome = this.goHome.bind(this);
        user = CurrentUserInfo.getCurrentUser();
        this.state = {
            value: 1,
            text: `Hello ${user.name},
                wait while we fetch info from server`,
            html: '',
            userState: 0
        };
    }
    handleChange(value) {
        optionId = value;
    }

    goHome() {
        this.props.history.push('/');
    }

    handleNext() {
        const cxt = this;
        console.log(optionId);
        switch (cxt.state.userState) {
            case 0: {
                axios.post('/setfoodprefs', {
                    id: user.id,
                    option: optionId
                }).then(function(response) {
                    console.dir(response.data);
                    cxt.setState({
                        text: `Hello ${user.name}, wait while we fetch info from server`,
                        html: '',
                    })
                    cxt.processStates();
                }).catch(function(error) {
                    console.log(error);
                });
                break;
            }
            case 3: {
                axios.post('/setusermeal', {
                    id: user.id,
                    option: optionId
                }).then(function(response) {
                    console.dir(response.data);
                    cxt.setState({
                        text: `Hello ${user.name}, wait while we fetch info from server`,
                        html: '',
                    })
                    cxt.processStates();
                }).catch(function(error) {
                    console.log(error);
                });
                break;
            }
        }
    }

    render() {
        return (
            <MuiThemeProvider>
              <div className='row'>
                <h2 className='header-text'> <a href='/'> BYL : Book Your Lunch </a> </h2>
                <div className='vspace'></div>
                <h5>{ this.state.text }</h5>
                { this.state.html }
                <div className='vspace'></div>
                <RaisedButton label="Go Home" primary={true} onClick={this.goHome} />
                <div className='vspace'></div>
              </div>
            </MuiThemeProvider>
            );
    }

    componentDidUpdate() {}
    componentDidMount() {
        this.processStates();
    }

    processStates() {
        const cxt = this;
        axios.post('/getuserstate', {
            id: user.id
        }).then(function (response) {
            console.log(optionId + '<== OptionId');
            console.dir(response.data);
            cxt.setState({
                userState: response.data.state
            })
            optionId = 1;
            UserStatesHTML.getUserStatesHTML(response.data.state, cxt);
        }).catch(function(error) {
            console.log(error);
        });
    }
}

import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SelectOptions from '../components/SelectOptions';
import RaisedButton from 'material-ui/RaisedButton';
import CurrentUserInfo from '../services/CurrentUserInfo';
import axios from 'axios';

/*
    Page Container for LandingPage
*/

let userId = 1;
let valueToUserIdMap = {};
let users;
export default class LandingPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleNext = this.handleNext.bind(this);
        this.state = {
            options: ''
        };
    }
    handleChange(value) {
        userId = valueToUserIdMap[value];
    }
    handleNext() {
        CurrentUserInfo.setCurrentUser(userId, users[userId][1]);
        if (userId == 1) {
            console.log(users[userId][1]);
            this.props.history.push('/chef');
        } else {
            console.log(users[userId][1]);
            this.props.history.push('/user');
        }
    }
    render() {
        return (
            <MuiThemeProvider>
              <div className='row landing-container'>
                <h2 className='header-text'> <a href='/'> BYL : Book Your Lunch </a></h2>
                <div className='vspace'></div>
                { this.state.options }
                <div className='vspace'></div>
                <RaisedButton label="Next" primary={ true } onClick={ this.handleNext } />
              </div>
            </MuiThemeProvider>
            );
    }

    componentDidMount() {
        const cxt = this;
        axios.get('/users')
            .then(function(response) {
                //console.dir(response);
                users = response.data.users;
                const menuItems = [];
                let i = 0;
                for (let p in users) {
                    i++;
                    valueToUserIdMap[i] = p;
                    menuItems.push(users[p][1]);
                }

                userId = valueToUserIdMap[1];
                cxt.setState({
                    options: <SelectOptions onChange={ cxt.handleChange } menuItems={ menuItems } label="Who are you ?" />
                });
                CurrentUserInfo.setCurrentUser(valueToUserIdMap[1], users[valueToUserIdMap[1]][1]);
            })
            .catch(function(error) {
                console.log(error);
            });
    }
}

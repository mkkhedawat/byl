import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const styles = {
    label: {
        textAlign: 'left',
        color: 'rgb(33, 150, 243)',
        fontSize: '1em',
        fontWeight: 300,
    },
    menu: {
        color: 'rgb(33, 150, 243)',
        fontWeight: 300
    }
};

export default class SelectOptions extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            value: 1,
        };
    }
    handleChange(event, index, value) {
        this.setState({
            value
        });
        this.props.onChange(value);
    }

    render() {
        return (
            <SelectField floatingLabelText={ this.props.label } floatingLabelStyle={ styles.label } value={ this.state.value } menuItemStyle={ styles.menu } onChange={ this.handleChange }
              autoWidth={ true }>
              { this.props.menuItems.map((item, index) => {
                    return (
                        <MenuItem key={ index + 1 } value={ index + 1 } primaryText={ item } />)
                }) }
            </SelectField>
            );
    }
}

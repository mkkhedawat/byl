# BYL
> Book your lunch

## How to run

```
- Clone the repo
- cd byl
- yarn ( or npm install )
- [Optional, Only run if changed React Src] npm run webpack
- node bin/www
```

## License

MIT © [Manish Kumar Khedawat](https://mkkhedawat.com)

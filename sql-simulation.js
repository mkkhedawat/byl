let chefSelectedMeals = [];

const users = {
    1: ['CHEF', 'Alena Stout'],
    2: ['USER', 'Sam Davies'],
    3: ['USER', 'Esther Sanchez'],
    4: ['USER', 'Corey Hopkins'],
    5: ['USER', 'Jamie Hamilton'],
    6: ['USER', 'Zak Butler'],
    7: ['USER', 'Louie Hart'],
    8: ['USER', 'Sophie Saunders'],
    9: ['USER', 'Amiyah Franco'],
    10: ['USER', 'Kylan Floyd'],
    11: ['USER', 'Brenton Marquez'],
    12: ['USER', 'Frank Butler'],
    13: ['USER', 'Rosie Lawson'],
    14: ['USER', 'Ava Thomson'],
    15: ['USER', 'Catalina Park']
}

/*
const users = {
    1: ['CHEF', 'Alena Stout'],
    2: ['USER', 'Sam Davies'],
    3: ['USER', 'Esther Sanchez'],
    4: ['USER', 'Corey Hopkins'],
}*/

const foodPreferences = {
    // food id : description
    1: 'Vegetarian',
    2: 'Gluten Free',
    3: 'Vegan',
    4: 'Nut Free',
    5: 'Diary Free',
    6: 'Eggs Free'
}

const mealCategories = {
    // food id : meal id

    1: [1, 3, 5, 7, 9],
    2: [],
    3: [3],
    4: [3, 4, 8, 9],
    5: [1, 2, 5, 6],
    6: [2, 8, 9]
}

const meals = {
    1: 'Sweetcorn Chilli and Chowder, Homous, Feta and Roast Vegetable Wrap',
    2: 'Tandoori Salmon with Bharji Crust',
    3: 'Chickpea Pancake with Cream Cheese Roast Tomatoes, and Asparagus',
    4: 'Lamb Kofta with Garlic Yogurt',
    5: 'Roast Aubergines with Harissa Spiced Chipeas',
    6: 'Orange and Hazel Glazed Nut Cornfed Chicken Supreme',
    7: 'Roast Peppers with Grilled Halloumi, Olives and Capers',
    8: 'Blue Cheese Beef Burger	Blackbean',
    9: 'Sweet Potato Burger with Blue Cheese'
}

const userFoodPreferences = {
    // user id : food pref id
    1: -1,
    2: -1,
    3: -1,
    4: -1,
    5: -1,
    6: -1,
    7: -1,
    8: -1,
    9: -1,
    10: -1,
    11: -1,
    12: -1,
    13: -1,
    14: -1,
    15: -1
}

const userMealSelections = {
    // user id : meal id
    1: -1,
    2: -1,
    3: -1,
    4: -1,
    5: -1,
    6: -1,
    7: -1,
    8: -1,
    9: -1,
    10: -1,
    11: -1,
    12: -1,
    13: -1,
    14: -1,
    15: -1
}

let recommendedMeals = [];

let chefMeals = [-1, -1, -1];

function getRecommendedMeals() {
    return recommendedMeals;
}

function setRecommendedMeals(meals) {
    recommendedMeals = meals;
}

function getFoodPreferences() {
    return foodPreferences;
}

function getFoodPreference(id) {
    return foodPreferences[id];
}

function getChefSelectedMeal() {
    return chefSelectedMeals;
}

function getMealCategories() {
    return mealCategories;
}

function getMeals() {
    return meals;
}

function getMeal(id) {
    return meals[id];
}


function getUsers() {
    return users;
}

function getUser(id) {
    return users[id];
}

function getUserFoodPreferences() {
    return userFoodPreferences;
}

function getUserFoodPreference(id) {
    return userFoodPreferences[id];
}

function setUserFoodPreference(userId, foodPrefId) {
    userFoodPreferences[userId] = foodPrefId;
    return true;
}

function getUserMealSelections() {
    return userMealSelections;
}

function getUserMealSelection(id) {
    return userMealSelections[id];
}

function setUserMealSelection(userId, mealId) {
    userMealSelections[userId] = mealId;
    return true;
}

function getChefMeals() {
    return chefMeals;
}

function setChefMeals(meal) {
    if (meal !== undefined && meal.length === 3) {
        chefMeals = [...meal];
    }
    return true;
}

module.exports = {
    getUsers,
    getUser,
    getFoodPreference,
    getFoodPreferences,
    getUserFoodPreferences,
    getUserFoodPreference,
    setUserFoodPreference,
    getChefMeals,
    setChefMeals,
    getUserMealSelection,
    setUserMealSelection,
    getUserMealSelections,
    getMeal,
    getMeals,
    getRecommendedMeals,
    setRecommendedMeals,
    getMealCategories
}